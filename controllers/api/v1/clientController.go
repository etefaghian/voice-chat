package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
	"voice-chat/services"
	"voice-chat/shared/models"
)

func clientController(controller *gin.RouterGroup) {
	controller.POST("", createClient)
	controller.GET("", readClients)
	controller.GET("/:id", readClient)
	controller.PUT("/:id", updateClient)
	controller.DELETE("/:id", deleteClient)
}

//delete a client
func deleteClient(context *gin.Context) {
	clientId := context.Param("id")
	err := services.DeleteClient(clientId)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}
	context.Status(200)

}

//update a client
//first we delete client from db and ws
//then we create a new client with same id or different id
func updateClient(context *gin.Context) {
	clientId := context.Param("id")
	err := services.DeleteClient(clientId)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}
	var request struct {
		Name     string `json:"name" `
		Username string `json:"username" bson:"username"`
		Password string `json:"password" bson:"password"`
		Role     string `json:"role" bson:"role"`
	}
	//give name from req body
	err = context.BindJSON(&request)
	//store info in db
	client := models.NewClientModel(request.Name, request.Username, request.Password, request.Role)
	//same id
	client.Id = clientId
	err = dao.CreateClient(client)
	//error handling
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
	} else {
		context.JSON(200, client)
	}

}

//get a client from clients
func readClient(context *gin.Context) {

	response, err := dao.ReadClient(context.Param("id"))
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}
	context.JSON(200, response)
}

//get all client from database
func readClients(context *gin.Context) {
	response, err := dao.ReadAllClients()
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
	} else {
		context.JSON(200, response)
	}
}

//add a client to db!!!!!
//get name from the client
//response models.client to the client
func createClient(context *gin.Context) {
	var request struct {
		Name     string `json:"name" `
		Username string `json:"username" bson:"username"`
		Password string `json:"password" bson:"password"`
		Role     string `json:"role" bson:"role"`
	}
	//give name from req body
	err := context.BindJSON(&request)
	//store info in db
	client := models.NewClientModel(request.Name, request.Username, request.Password, request.Role)
	err = dao.CreateClient(client)
	//error handling
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"err": err.Error(),
		})
	} else {
		context.JSON(200, client)
	}
}
