package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"voice-chat/controllers/api/v1/middlewares/accessControlOriginMiddleware"
)

func init() {
	RunServer()
}

//get authorization server address  from security package and assign it to server variable in ginserver third party package

func RunServer() {
	gin.ForceConsoleColor()
	r := gin.Default()
	//solve angular problem
	r.Use(accessControlOriginMiddleware.AccessControlOriginMiddleware)

	api := r.Group("/api/v1")
	setupRoutes(api)
	r.MaxMultipartMemory = 2 << 30 * 5

	err := r.Run(":5000")
	if err != nil {
		logrus.Error(err)
	}
}

func setupRoutes(api *gin.RouterGroup) {
	wsController(api.Group("/ws"))
	wsStatusController(api.Group("/ws_status"))
	clientController(api.Group("/clients"))
	fileController(api.Group("/files"))
	hubController(api.Group("/hubs"))
	oauthController(api.Group("/oauth2"))
}
