package controllers

import (
	"github.com/gin-gonic/gin"
	"io"
	"mime/multipart"
	"voice-chat/services"
)

//start point
func fileController(controller *gin.RouterGroup) {
	controller.POST("/upload", func(context *gin.Context) {
		fh, err := context.FormFile("image")
		if err == nil {
			imageUploadController(fh, context)
		} else {
			context.JSON(400, gin.H{"err": err.Error()})
			return
		}
	})

	//get id form json and return image
	controller.POST("/download", func(context *gin.Context) {
		var request struct {
			Id string `json:"id"`
		}
		err := context.BindJSON(&request)
		if err != nil {
			context.JSON(400, gin.H{"err": err.Error()})
			return
		}
		//f, err := os.Open()

	})
}

//helper method
func imageUploadController(fh *multipart.FileHeader, context *gin.Context) {
	//create a file from fh
	//is not os.file
	file, err := fh.Open()
	if err != nil {
		context.JSON(400, gin.H{"err": err.Error()})
		return
	}
	var bytesOfFile = make([]byte, fh.Size)
	_, err = file.Read(bytesOfFile)
	if err != nil && err != io.EOF {
		context.JSON(400, gin.H{"err": err.Error()})
		return
	}
	response, err := services.UploadImage(bytesOfFile, fh.Filename, "")
	if err != nil {
		context.JSON(400, gin.H{"err": err.Error()})
		return
	}
	context.JSON(200, response)

}
