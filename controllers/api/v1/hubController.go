package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
	"voice-chat/dao"
	"voice-chat/services"
	"voice-chat/shared/models"
)

type requestCreateHub struct {
	Name          string   `json:"name"`
	ClientsId     []string `json:"clients_id"`
	OperatorId    string   `json:"operator_id"`
	AdminId       string   `json:"admin_id"`
	ConstraintsId []string `json:"constraints_id"`
}

func hubController(controller *gin.RouterGroup) {
	controller.POST("", createHub)
	controller.DELETE(":id", deleteHub)
	controller.GET("", readHubs)
	controller.GET("/:id", readHub)
	controller.GET("/:id/public-messages", readPublicMessages)
	controller.GET("/:id/private-messages/:client", readPrivateMessage)
	controller.PUT("/:id", updateHub)
}

//read private message for a hub peer to peer message
//time format is RFC3339
//page start from 1 default is 1
//but in db pages start from 0
//default for limit is 20
func readPrivateMessage(context *gin.Context) {
	hubId := context.Param("id")
	clientId := context.Param("client")
	page, err := strconv.Atoi(context.DefaultQuery("page", "0"))
	date, err := time.Parse(time.RFC3339, context.Query("date"))
	limit, err := strconv.Atoi(context.DefaultQuery("limit", "20"))
	private, err := dao.ReadPrivateMessagesOfClientFromHubAfter(hubId, clientId, date, page-1, limit)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
	} else {
		context.JSON(200, private)
	}
}

//read a hub
func readHub(context *gin.Context) {
	response, err := dao.ReadHub(context.Param("id"))
	if err != nil {
		context.JSON(400, err)
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
	} else {
		context.JSON(200, response)
	}
}

//read public message for a hub
//time format is RFC339
//page start from 1 default is 1
//default for limit is 20
func readPublicMessages(context *gin.Context) {
	hubId := context.Param("id")
	page, err := strconv.Atoi(context.DefaultQuery("page", "1"))
	date, err := time.Parse(time.RFC3339, context.Query("date"))
	limit, err := strconv.Atoi(context.DefaultQuery("limit", "20"))
	public, err := dao.ReadPublicMessagesFromHubAfter(hubId, date, page-1, limit)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
	} else {
		context.JSON(200, public)
	}
}

//read all hubs
func readHubs(context *gin.Context) {
	response, err := dao.ReadAllHubs()
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	context.JSON(200, response)
}

//delete a hub
func deleteHub(context *gin.Context) {
	id := context.Param("id")
	err := services.DeleteHub(id)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	context.Status(200)

}

//create a hub
func createHub(context *gin.Context) {
	//make request that from the client
	var request requestCreateHub
	err := context.BindJSON(&request)

	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	hub := models.NewHubModel(
		request.Name,
		request.ClientsId,
		request.OperatorId,
		request.AdminId,
		request.ConstraintsId)

	//use service to add hub to db
	err = services.CreateHub(hub)

	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	context.JSON(200, hub)
}

//update a hub from hub db
func updateHub(context *gin.Context) {
	var request requestCreateHub
	err := context.BindJSON(&request)
	if err != nil {
		logrus.Error(err)
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	newHub := models.NewHubModel(
		request.Name,
		request.ClientsId,
		request.OperatorId,
		request.AdminId,
		request.ConstraintsId)

	id := context.Param("id")
	err = services.UpdateHub(id, newHub)
	if err != nil {
		context.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	newHub.Id = id
	context.Status(200)
}
