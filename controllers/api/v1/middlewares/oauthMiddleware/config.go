package oauthMiddleware

import (
	ginserver "github.com/go-oauth2/gin-server"
	"github.com/sirupsen/logrus"
	"gopkg.in/oauth2.v3/server"
	"voice-chat/services/security"
)

var AuthorizationServer *server.Server

func init() {
	AuthorizationServerInit()
}

func AuthorizationServerInit() {
	AuthorizationServer = security.AuthorizationServer
	addressOfGinServer := ginserver.InitServer(nil)
	addressOfGinServer = AuthorizationServer
	logrus.Info(addressOfGinServer)
}
