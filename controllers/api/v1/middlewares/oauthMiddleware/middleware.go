package oauthMiddleware

import (
	"github.com/gin-gonic/gin"
	ginserver "github.com/go-oauth2/gin-server"
	"net/http"
	"voice-chat/dao"
)

//an authorization middleware
func AuthorizationMiddleware(roles ...string) gin.HandlerFunc {
	return func(context *gin.Context) {
		//get token info
		token, err := AuthorizationServer.ValidationBearerToken(context.Request)
		//err means user is not authorized
		if err != nil {
			context.Status(http.StatusUnauthorized)
			context.Abort()
			return
		}
		//get user id from token information
		id := token.GetUserID()
		client, err := dao.ReadClient(id)
		//client is not exist deleted or .....
		if err != nil {
			context.Status(http.StatusUnauthorized)
			context.Abort()
			return
		}
		//if array of roles be empty
		//with no role parameter
		if len(roles) == 0 {
			context.Next()
			return
		}
		//check role parameter is not violate
		for _, role := range roles {
			if role == client.Role {
				context.Next()
				return
			}
		}
		context.Status(http.StatusForbidden)
		context.Abort()
	}
}

func GenerateAccessToken() func(ctx *gin.Context) {
	return ginserver.HandleTokenRequest
}
