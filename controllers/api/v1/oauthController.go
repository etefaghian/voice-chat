package controllers

import (
	"github.com/gin-gonic/gin"
	"voice-chat/controllers/api/v1/middlewares/oauthMiddleware"
)

func oauthController(controller *gin.RouterGroup) {
	controller.GET("/token", oauthMiddleware.GenerateAccessToken())
}
