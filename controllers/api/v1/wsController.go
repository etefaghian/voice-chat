package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
	"voice-chat/services/ws"
	"voice-chat/shared/models"
)

func wsController(controller *gin.RouterGroup) {

	//all thing will start from here!!!!
	//web socket!
	controller.GET("/:id", func(c *gin.Context) {
		id := c.Param("id")
		var clientModel models.ClientModel
		var err error
		clientModel, err = dao.ReadClient(id)
		if err != nil {
			logrus.Error(err)
			c.JSON(400, err)
			return
		}
		ws.ServeClient(clientModel.Id, clientModel.Name, c.Writer, c.Request)
	})
}
