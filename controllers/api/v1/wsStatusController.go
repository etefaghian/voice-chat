package controllers

import (
	"github.com/gin-gonic/gin"
	"voice-chat/services/ws"
)

func wsStatusController(controller *gin.RouterGroup) {
	controller.GET("/hubs", readHubsInWS)
	controller.GET("/clients", readClientsInWS)

	controller.GET("/clients/:id", func(context *gin.Context) {
		client, err := ws.ReadClientFromWS(context.Param("id"))
		if err != nil {
			context.Status(400)
			return
		}
		context.JSON(200, gin.H{
			"id":   client.Id,
			"name": client.Name,
			"hubs": client.Hubs,
		})
	})
}

func readClientsInWS(context *gin.Context) {
	clients, _ := ws.StatusOfClientsInWS()
	context.JSON(200, clients)

}

func readHubsInWS(context *gin.Context) {
	hubs, _ := ws.StatusOfHubsInWS()
	context.JSON(200, hubs)

}
