package dao

import (
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"voice-chat/shared/models"
)

//read one client
func ReadClient(id string) (models.ClientModel, error) {
	var result models.ClientModel
	singleResult := clientsCollection.FindOne(ctx, bson.D{
		{"_id", id},
	})
	err := singleResult.Decode(&result)
	if err != nil {
		return result, err
	}
	return result, nil
}

func ReadClientByUsername(username string) (models.ClientModel, error) {
	var result models.ClientModel
	singleResult := clientsCollection.FindOne(ctx, bson.D{
		{"username", username},
	})
	err := singleResult.Decode(&result)
	if err != nil {
		return result, err
	}
	return result, nil
}

//read all clients
func ReadAllClients() ([]models.ClientModel, error) {
	var result []models.ClientModel
	cursor, err := clientsCollection.Find(ctx, bson.D{})
	defer cursor.Close(ctx)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	err = cursor.All(ctx, &result)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer cursor.Close(ctx)
	return result, nil
}

//create client with client model
func CreateClient(client models.ClientModel) error {
	_, err := clientsCollection.InsertOne(ctx, client)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//delete a client from collection
func DeleteClient(id string) error {
	_, err := clientsCollection.DeleteOne(ctx, bson.D{
		{"_id", id},
	})
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

func AddHubsToClient(clientId string, hubsId []string) error {
	query := bson.M{"_id": clientId}
	//add to set of hub id
	update := bson.M{"$addToSet": bson.M{"hubs_id": bson.M{"$each": hubsId}}}
	_, err := clientsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//read array of hubs for specified client
func ReadHubsOfClient(id string) ([]string, error) {
	var model struct {
		HubsId []string `bson:"hubs_id" json:"hubs_id"`
	}
	//show a hide some field
	pipeline := make([]bson.M, 0)
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"_id": id,
		},
	})
	pipeline = append(pipeline, bson.M{
		"$project": bson.M{
			"_id":     0,
			"hubs_id": 1,
		},
	})
	cursor, err := clientsCollection.Aggregate(ctx, pipeline)
	defer cursor.Close(ctx)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	if cursor.Next(ctx) {
		err = cursor.Decode(&model)
	}
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	return model.HubsId, nil
}

//check whether is a hub in client db or not
func ExistsHubForClient(clientId, hubId string) bool {
	hubs, err := ReadHubsOfClient(clientId)
	if err != nil {
		logrus.Error(err)
		return false
	}
	for _, hub := range hubs {
		if hub == hubId {
			return true
		}
	}
	return false
}

//remove hubs from a client in client collection
func RemoveHubsFromClient(clientId string, hubsId []string) error {
	query := bson.M{
		"_id": clientId,
	}
	update := bson.M{
		"$pull": bson.M{
			"hubs_id": bson.M{"$in": hubsId},
		},
	}

	_, err := clientsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

func UpdateClient(id string, object interface{}) error {
	query := bson.D{
		{"_id", id},
	}
	_, err := clientsCollection.UpdateOne(ctx, query, object)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
