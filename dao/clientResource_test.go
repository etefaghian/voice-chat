package dao

import "testing"

func TestRemoveHubsFromClient(t *testing.T) {
	type args struct {
		clientId string
		hubsId   []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{"1", args{"5e791f358b83fdcba7167068", []string{"1", "2", "3"}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := RemoveHubsFromClient(tt.args.clientId, tt.args.hubsId); (err != nil) != tt.wantErr {
				t.Errorf("RemoveHubsFromClient() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
