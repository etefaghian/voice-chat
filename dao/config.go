package dao

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

//use in whole the app
var ctx context.Context

//connection to db
var client *mongo.Client

const DataBaseName = "voice-chat"

const uri = "mongodb://localhost:27017"

//const uri = "mongodb+srv://etefagh:1998@cluster0-1vt7y.mongodb.net/test?retryWrites=true&w=majority"
//use for collections
//we use it in dao
var clientsCollection *mongo.Collection
var hubsCollection *mongo.Collection
var fileCollection *mongo.Collection

func init() {
	//cancel var is omitted
	ctx := context.TODO()
	var err error
	client, err = mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatalln(err)
	}
	//init collections
	setUpCollection()
}

//init collections that defined in bellow
func setUpCollection() {
	clientsCollection = client.Database(DataBaseName).Collection("clients")
	hubsCollection = client.Database(DataBaseName).Collection("hubs")
	fileCollection = client.Database(DataBaseName).Collection("files")

}
