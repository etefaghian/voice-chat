package dao

import (
	"github.com/sirupsen/logrus"
	"voice-chat/shared/models"
)

func CreateFile(file models.FileModel) error {
	_, err := fileCollection.InsertOne(ctx, file)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//in future
func ReadFile() {

}

//in future
func DeleteFile() {

}
