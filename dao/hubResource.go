package dao

import (
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
	"voice-chat/shared/models"
)

func CreateHub(hub models.HubModel) error {
	_, err := hubsCollection.InsertOne(ctx, hub)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

//read all hubs
func ReadAllHubs() ([]models.HubModel, error) {
	var returnValue []models.HubModel
	cursor, err := hubsCollection.Find(ctx, bson.D{}, options.Find().SetProjection(bson.M{
		"private_messages": 0,
		"public_messages":  0,
	}))
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	err = cursor.All(ctx, &returnValue)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	return returnValue, nil
}

func ReadHub(id string) (models.HubModel, error) {
	var returnValue models.HubModel
	singleResult := hubsCollection.FindOne(ctx, bson.M{"_id": id}, options.FindOne().SetProjection(bson.M{
		"private_messages": 0,
		"public_messages":  0,
	}))
	err := singleResult.Decode(&returnValue)
	if err != nil {
		logrus.Error(err)
		return models.HubModel{}, err
	}
	return returnValue, nil
}

func ReadPublicMessagesFromHubAfter(hubId string, timestamp time.Time, page int, limit int) ([]models.GeneralMessageModel, error) {
	//for aggregation framework
	var pipeline = make([]bson.M, 0)

	//a temp for message
	var messages []struct {
		PublicMessages models.GeneralMessageModel `bson:"public_messages"`
	}

	//stage 1 match specified hub
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"_id": hubId,
		},
	})

	//stage 2 projection only private_messages
	pipeline = append(pipeline, bson.M{
		"$project": bson.M{
			"_id":             0,
			"public_messages": 1,
		},
	})

	//stage 3 hello to unwind 1 doc 1 array to arrays of docs
	pipeline = append(pipeline, bson.M{
		"$unwind": "$public_messages",
	})

	//stage 4 filter by date
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"public_messages.timestamp": bson.M{"$gt": timestamp}},
	})

	//stage 6 sort based on timestamp desc
	pipeline = append(pipeline, bson.M{
		"$sort": bson.M{
			"public_messages.timestamp": -1,
		},
	})

	//stage 7 skip messages
	pipeline = append(pipeline, bson.M{
		"$skip": (page) * limit,
	})

	//stage 8 limit messages
	pipeline = append(pipeline, bson.M{
		"$limit": limit,
	})

	//aggregation framework
	cursor, err := hubsCollection.Aggregate(ctx, pipeline)
	defer cursor.Close(ctx)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	//write result in messages variable
	err = cursor.All(ctx, &messages)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	//make return value based on []messages
	var returnValue = make([]models.GeneralMessageModel, 0)
	for _, v := range messages {
		returnValue = append(returnValue, v.PublicMessages)
	}
	//end!!!!!!!!
	return returnValue, nil

}

//read private message in special condition
func ReadPrivateMessagesOfClientFromHubAfter(hubId string, clientId string, timestamp time.Time, page int, limit int) ([]models.GeneralMessageModel, error) {
	//for aggregation framework
	var pipeline = make([]bson.M, 0)

	//a temp for message
	var messages []struct {
		PrivateMessages models.GeneralMessageModel `bson:"private_messages"`
	}

	//stage 1 match specified hub
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"_id": hubId,
		},
	})

	//stage 2 projection only private_messages
	pipeline = append(pipeline, bson.M{
		"$project": bson.M{
			"_id":              0,
			"private_messages": 1,
		},
	})

	//stage 3 hello to unwind 1 doc 1 array to arrays of docs
	pipeline = append(pipeline, bson.M{
		"$unwind": "$private_messages",
	})

	//stage 4 filter by date
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"private_messages.timestamp": bson.M{"$gt": timestamp}},
	})

	//stage 5 filter by client_id in sender or receiver
	pipeline = append(pipeline, bson.M{
		"$match": bson.M{
			"$or": bson.A{
				bson.M{"private_messages.receiver_id": clientId},
				bson.M{"private_messages.sender_id": clientId},
			},
		},
	})

	//stage 6 sort based on timestamp desc
	pipeline = append(pipeline, bson.M{
		"$sort": bson.M{
			"private_messages.timestamp": -1,
		},
	})

	//stage 7 skip messages
	pipeline = append(pipeline, bson.M{
		"$skip": (page) * limit,
	})

	//stage 8 limit messages
	pipeline = append(pipeline, bson.M{
		"$limit": limit,
	})

	//aggregation framework
	cursor, err := hubsCollection.Aggregate(ctx, pipeline)
	defer cursor.Close(ctx)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	//write result in messages variable
	err = cursor.All(ctx, &messages)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	//make return value based on []messages
	var returnValue = make([]models.GeneralMessageModel, 0)
	for _, v := range messages {
		returnValue = append(returnValue, v.PrivateMessages)
	}
	//end!!!!!!!!
	return returnValue, nil
}

func AddPublicMessageToHub(hubId string, message models.GeneralMessageModel) error {
	query := bson.M{"_id": hubId}
	update := bson.M{"$addToSet": bson.M{"public_messages": message}}
	_, err := hubsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

func AddPrivateMessageToHub(hubId string, message models.GeneralMessageModel) error {
	query := bson.M{"_id": hubId}
	update := bson.M{"$addToSet": bson.M{"private_messages": message}}
	_, err := hubsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

func DeleteHub(id string) error {
	_, err := hubsCollection.DeleteOne(ctx, bson.M{
		"_id": id,
	})
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//add clients to Hub
//give an array of client id in string format
func AddClientsToHub(id string, clientsId []string) error {
	query := bson.M{"_id": id}
	//add to set of hub id
	update := bson.M{"$addToSet": bson.M{"clients_id": bson.M{"$each": clientsId}}}
	_, err := hubsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil

}

func RemoveClientsFromHub(hubId string, clientsId []string) error {
	query := bson.M{
		"_id": hubId,
	}
	update := bson.M{
		"$unset": bson.M{
			"clients_id": bson.M{"$in": clientsId},
		},
	}
	_, err := clientsCollection.UpdateOne(ctx, query, update)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil

}
