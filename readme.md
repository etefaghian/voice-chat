# Chat Application
 

## Requirements for running project
- Go language
- Mongo DB
- A Mongo DB client for example 
mongo compose (optional)
- Postman or any similar app
- Firecamp or any client for testing websocket
- GOOD MOOD

## Running project
with  `go run main.go` we can run project

first run take a little time for download dependency  

## Correct workflow in api
- create clients with rest
- create hub with rest 
- connect to ws to send messages to each other

## Structure of  data transmission Message in WS and REST
### REST API
For rest api we need  to add a collection to postman 
collection now available in  
https://www.getpostman.com/collections/b04f75c6f2d43fc3a401





### Web Socket Message Structure
ws is available in 
`localhost:5000/api/v1/ws/{id}
` that id is variable 

#### Message Structure in WS
`{
"receiver_id":"",
"hub_id":"5df0bee053a73fcf79eebc96",
"code":2,
"body":"kowefwkofkfk"
}`

- receiver_id : a client id that receives a message if that is empty means public message
(broadcast message)
- hub_id : specified hub that message belong it
- code : in number data type for error handling and etc
- body : body of message in string format


##~~**Warning: this section for Developer(for example ETE) only**~~
###Hub Collection
####Create hub 
for create hub we should do some steps:
1. Create a new hub in db
2. Add new hub to client collection in db
3. Add hub id to the clients that connected to ws
#####Read clients
 




