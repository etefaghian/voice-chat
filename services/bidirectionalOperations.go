package services

import (
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
)

//in 2 steps
//1 add the clients to the hub
//2 add a hub to the clients
func bidirectionalAddClientsToHub(hubId string, clientsId []string) error {
	err := dao.AddClientsToHub(hubId, clientsId)
	if err != nil {
		logrus.Error(err)
	}
	err = unidirectionalAddAHubToSomeClientsInDb(hubId, clientsId)
	if err != nil {
		logrus.Error(err)
	}
	return err
}

//in 2 steps
//remove clients form hub
//remove hub from each client
func bidirectionalRemoveClientsFromHub(hubId string, clientsId []string) error {
	err := dao.RemoveClientsFromHub(hubId, clientsId)
	if err != nil {
		logrus.Error(err)
	}
	err = unidirectionalRemoveAHubFromSomeClientsInDb(hubId, clientsId)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
