package services

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
	"voice-chat/services/ws"
	"voice-chat/shared/models"
)

// delete a client from db and websocket
//step 1 remove client from websocket
//step 2 remove the client from all associated hub
//step 3 delete client from client collection
func DeleteClient(clientId string) error {
	//find client entity from db
	client, err := dao.ReadClient(clientId)
	if err != nil {
		logrus.Error(err)
		return err
	}

	clientPointer, err := ws.ReadClientFromWS(clientId)
	//step 1
	if clientPointer != nil {
		logrus.Debug("find client pointer")

		err := clientPointer.BidirectionalDeleteClientFromWS()
		if err != nil {
			logrus.Error(err)
			return err
		}
	}
	fmt.Print("in delete client in db")

	//step 2
	for _, hubId := range client.HubsId {
		err = bidirectionalRemoveClientsFromHub(hubId, []string{clientId})
	}
	//step 3
	err = dao.DeleteClient(clientId)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

func UpdateClient(clientId string, newClient models.ClientModel) error {
	err := DeleteClient(clientId)
	if err != nil {
		logrus.Error(err)
		return err
	}
	newClient.Id = clientId
	err = dao.CreateClient(newClient)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
