package services

import (
	"github.com/sirupsen/logrus"
	"io"
	"os"
	"voice-chat/dao"
	"voice-chat/shared/models"
)

var imagePath = "files/images/"

func init() {
	os.MkdirAll(imagePath, os.ModeDir)
}

func UploadImage(image []byte, name string, clientId string) (models.FileModel, error) {
	model := models.NewImageModel(name, clientId)
	err := dao.CreateFile(model)
	if err != nil {
		logrus.Error(err)
		return models.FileModel{}, err
	}
	file, err := os.Create(imagePath + model.Id + ".jpg")
	defer func() {
		_ = file.Close()
	}()
	if err != nil {
		logrus.Error(err)
		return models.FileModel{}, err
	}
	_, err = file.Write(image)
	if err != nil && err != io.EOF {
		logrus.Error(err)
		return models.FileModel{}, err
	}
	return model, nil
}

//func DownloadImageWithInformation(id string) ([]byte, models.FileModel, error) {
//}

//func DownloadImageWithoutInformation(id string) ([]byte, models.FileModel, error) {

//}
