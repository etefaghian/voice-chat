package services

import (
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
	"voice-chat/services/ws"
	"voice-chat/shared/models"
)

//in 3 step
//1 create hub in db
//2 add hub to the clients in db
//3 check if clients exist in ws add hub to them in ws scope
func CreateHub(hub models.HubModel) error {
	//step 1
	//create hub with all information in db
	err := dao.CreateHub(hub)
	if err != nil {
		logrus.Error(err)
	}
	//add the hub to all client that are member of hubs
	//in client collection
	err = unidirectionalAddAHubToSomeClientsInDb(hub.Id, hub.ClientsId)
	if err != nil {
		logrus.Error(err)
	}
	//check in the clients connect to ws add hub into client there
	err = ws.AddAHubToClientsInWs(hub.Id, hub.ClientsId)
	if err != nil {
		logrus.Error(err)
	}
	return err
}

//in 3 step
//1 stop hub if the hub is running in ws
//2 remove the hub from clients that the clients has been added the hub
//3 delete the hub from db
func DeleteHub(hubId string) error {
	//step 1
	if err := ws.RemoveHubFromWs(hubId); err != nil {
		logrus.Error(err)
	}

	//step 2
	hub, err := dao.ReadHub(hubId)
	if err != nil {
		logrus.Error(err)
	}
	clientsId := hub.ClientsId
	//remove the hub from each associated client
	err = unidirectionalRemoveAHubFromSomeClientsInDb(hubId, clientsId)
	//step 3
	err = dao.DeleteHub(hubId)
	if err != nil {
		logrus.Error(err)
	}

	return err
}

func UpdateHub(hubId string, newHub models.HubModel) error {
	err := DeleteHub(hubId)
	if err != nil {
		return err
	}

	newHub.Id = hubId
	err = CreateHub(newHub)
	if err != nil {
		logrus.Error(err)
		return err
	}

	return nil
}
