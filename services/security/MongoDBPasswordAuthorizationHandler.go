package security

import (
	"fmt"
	"voice-chat/dao"
)

func MongoDBPasswordAuthorization(username string, password string) (userID string, err error) {
	client, err := dao.ReadClientByUsername(username)
	if err != nil {
		return "", fmt.Errorf("user not found")
	}
	if password != client.Password {
		return "", fmt.Errorf("password is not correct")
	}
	return client.Id, nil
}
