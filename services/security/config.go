package security

import (
	ginserver "github.com/go-oauth2/gin-server"
	"gopkg.in/oauth2.v3"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

//use in authorizationServer controller
var AuthorizationServer *server.Server

func init() {
	configAuthorizationServer()
}

func configAuthorizationServer() {
	manager := manage.NewDefaultManager()
	// token store
	manager.MustTokenStorage(store.NewFileTokenStore("data.db"))
	// client store

	clientStore := store.NewClientStore()
	clientStore.Set("000000", &models.Client{
		ID:     "000000",
		Secret: "android",
		Domain: "http://localhost",
	})

	manager.MapClientStorage(clientStore)
	// Initialize the oauth2 service
	//address reference
	AuthorizationServer = ginserver.InitServer(manager)
	ginserver.SetAllowGetAccessRequest(true)
	ginserver.SetAllowedGrantType(oauth2.PasswordCredentials, oauth2.ClientCredentials, oauth2.Refreshing)
	AuthorizationServer.SetClientInfoHandler(server.ClientFormHandler)
	ginserver.SetPasswordAuthorizationHandler(MongoDBPasswordAuthorization)
}
