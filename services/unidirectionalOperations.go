package services

import (
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
)

//remove hubId from each client in []clients
func unidirectionalRemoveAHubFromSomeClientsInDb(hubId string, clientsId []string) error {
	var err error
	for _, clientId := range clientsId {
		err = dao.RemoveHubsFromClient(clientId, []string{hubId})
	}
	if err != nil {
		logrus.Error(err)
		return err
	}

	return nil
}

//add a hubId to clients in client collection
func unidirectionalAddAHubToSomeClientsInDb(hubId string, clientsId []string) error {
	var err error
	for _, clientId := range clientsId {
		err = dao.AddHubsToClient(clientId, []string{hubId})
	}
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
