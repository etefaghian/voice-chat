package ws

import (
	"voice-chat/shared/models"
)

//be care
//if we change package of message generator we face with circular dependency

//construct a message for send to sender that ignore access level policy
func generateAccessLevelErrorMessage(input models.GeneralMessageModel) models.GeneralMessageModel {
	input.Body = "Access Level Error Message"
	input.Code = accessLevelErrorCode
	return input
}
func generateHubNotFoundErrorMessage(input models.GeneralMessageModel) models.GeneralMessageModel {
	input.Code = hubNotFoundErrorCode
	input.Body = "hub Id Not Found"
	return input
}
func generateReceiverNotFoundErrorMessage(input models.GeneralMessageModel) models.GeneralMessageModel {
	input.Code = receiverNotFoundErrorCode
	input.Body = "Receiver Id Not Found"
	return input

}
func generateReceiverFoundButNotConnectedErrorMessage(input models.GeneralMessageModel) models.GeneralMessageModel {
	input.Code = receiverFoundButNotConnectedErrorCode
	input.Body = "Receiver Id Found But Is Not connected"
	return input

}
