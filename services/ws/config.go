package ws

import (
	"github.com/sirupsen/logrus"
	"os"
)

//unused now
func createImageDir() error {
	err := os.Mkdir("images", os.ModePerm)
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
