package ws

//code for return back to client
const (
	accessLevelErrorCode                  = 410
	hubNotFoundErrorCode                  = 420
	receiverNotFoundErrorCode             = 430
	receiverFoundButNotConnectedErrorCode = 440
)
