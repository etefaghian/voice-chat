package ws

import (
	"fmt"
	"voice-chat/shared/models"
)

func checkAccessLevel(hub *hub, message models.GeneralMessageModel) (models.GeneralMessageModel, error) {

	if (message.SenderId == hub.OperatorId || message.SenderId == hub.AdminId) || (message.ReceiverId == hub.OperatorId || message.ReceiverId == hub.AdminId) {
		return message, nil
	}
	return generateAccessLevelErrorMessage(message), fmt.Errorf("")
}
