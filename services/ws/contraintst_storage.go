package ws

import (
	"voice-chat/shared/models"
)

var constraintsStorage = make(map[string]func(hub *hub, model models.GeneralMessageModel) (models.GeneralMessageModel, error))

//init constraint storage
func init() {

	constraintsStorage["1"] = checkAccessLevel
}

func readConstraint(id string) func(hub *hub, model models.GeneralMessageModel) (models.GeneralMessageModel, error) {
	//if exists
	if constraintsStorage[id] != nil {
		return constraintsStorage[id]
	}
	//if not exists
	return nil
}
func existsConstraint(id string) bool {
	//if exists
	if constraintsStorage[id] != nil {
		return true
	}
	//if not exists
	return false
}
