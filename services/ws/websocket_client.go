package ws

import (
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
	"voice-chat/dao"
	"voice-chat/shared/models"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second
	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second
	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	Hubs map[string]*hub
	// The websocket connection.
	conn *websocket.Conn
	// Buffered channel of outbound messages.
	send chan models.GeneralMessageModel
	//identifier for client
	Name string
	Id   string
}

//write terminateExecution message in web socket
//terminateExecution tcp connection in infrastructure
func (c *Client) terminateConnection() error {
	logrus.Warn("terminateExecution connection of client:  ", c.Name, " ", c.Id)
	err := c.conn.WriteMessage(websocket.CloseMessage, []byte{})
	err = c.conn.Close()
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//set limit and pong handler for websocket
func (c *Client) initializeForReadFromWebSocket() {
	c.conn.SetReadLimit(maxMessageSize)              //excess from max size terminateExecution connection
	c.conn.SetReadDeadline(time.Now().Add(pongWait)) //init time for  reading
	c.conn.SetPongHandler(func(string) error {       //pong handler
		logrus.Info("in pong handler client: ", "  ", c.Name, "  ", c.Id)
		c.conn.SetReadDeadline(time.Now().Add(pongWait)) //add more time for reading
		return nil
	})
}

//read messages form web socket
//handle ping message for checking alive
func (c *Client) readFromWebSocket() {
	defer func() {
		logrus.Warn("terminateExecution reading of client:  ", c.Name, " ", c.Id)
		_ = c.BidirectionalDeleteClientFromWS()
	}()
	//pong handler and limit
	c.initializeForReadFromWebSocket()

	for {
		//fill message in next line
		//here we fill only sender id correspond
		var message = models.NewGeneralMessageModel(c.Id, "", "", -1, nil)
		err := c.conn.ReadJSON(&message)
		logrus.Info("in read message:", message)
		if err != nil {
			return
		}
		//if hub found in Hubs array of client send message to hub
		//else generate hubNotFoundErrorMessage
		if _, ok := c.Hubs[message.HubId]; ok {
			c.Hubs[message.HubId].Broadcast <- message
		} else {
			c.send <- generateHubNotFoundErrorMessage(message)
		}
	}
}
func (c *Client) InitConfig() {
	c.conn.SetCloseHandler(func(code int, text string) error {
		err := c.BidirectionalDeleteClientFromWS()
		return err
	})
}

//write data in client side of web socket
func (c *Client) writeInWebSocket() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		logrus.Warn("terminateExecution writing of client:  ", c.Name, " ", c.Id)
		_ = c.BidirectionalDeleteClientFromWS()
		ticker.Stop()
	}()
	for {

		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the web socket
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			err := c.conn.WriteJSON(message)
			logrus.Info("message is written for client:  ", c.Name, "  ", c.Id)
			if err != nil {
				logrus.Error(err)
				return
			}
		case <-ticker.C:
			logrus.Info("ping production for client: ", c.Name, "  ", c.Id)
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			err := c.conn.WriteMessage(websocket.PingMessage, []byte{})
			if err != nil {
				logrus.Error(err)
				return
			}
		}

	}
}

//beginning point for websocket
func ServeClient(id string, name string, w http.ResponseWriter, r *http.Request) { // serveWs handles websocket requests from the peer.
	//upgrade connection in websocket
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.Error(err)
		return
	}

	client := &Client{Hubs: make(map[string]*hub),
		conn: conn,
		send: make(chan models.GeneralMessageModel, 256),
		Id:   id,
		Name: name,
	}
	addClientToClientStorage(client.Id, client)
	err = client.runHubsOfClientFromDB()
	if err != nil {
		logrus.Error(err)
	}
	client.InitConfig()
	go client.writeInWebSocket()
	go client.readFromWebSocket()
}

//read Hubs of the client from db in first running
//add the client to own Hubs
func (c *Client) runHubsOfClientFromDB() error {
	HubsFromDb, err := dao.ReadHubsOfClient(c.Id)
	if err != nil {
		logrus.Error(err)
		return err
	}
	for _, hub := range HubsFromDb {
		err = c.bidirectionalAddToAHub(hub) // do not set operator and admin
	}
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}

//add the client to specified hub in ws
//bidirectional
//add the client to the hub
//add the hub to the client array
func (c *Client) bidirectionalAddToAHub(hubId string) error {
	hub, err := dao.ReadHub(hubId) //get information of a hub from db
	if err != nil {
		logrus.Error(err)
		return err
	}
	//if the hub is already exists return address of hub
	//else create new hub
	c.Hubs[hubId] = serveHub(hub.Id, hub.Name, hub.OperatorId, hub.AdminId, hub.ConstraintsId)
	//register client to the hub
	c.Hubs[hubId].Register <- c
	return nil
}

//remove client from its Hubs
//stop two goroutine
//remove client form client storage
func (c *Client) BidirectionalDeleteClientFromWS() error {
	removeClientFromClientStorage(c.Id)
	var err error
	for hubId, _ := range c.Hubs {
		err := c.BidirectionalRemoveFromHub(hubId)
		logrus.Error(err)
	}
	//terminateExecution infrastructure for client ( tcp connection)
	err = c.terminateConnection()
	if err != nil {
		logrus.Error(err)
	}
	//terminateExecution(c.send)
	return err
}

//remove the client from specified hub
//step 1 remove client from  specified hub in hub side
//step 2 remove specified hub from the client in client side
func (c *Client) BidirectionalRemoveFromHub(hubId string) error {
	logrus.Info("in bidirectional remove for client: ", c.Id)
	//delete client from the hub
	//delete hub from the client
	if _, ok := c.Hubs[hubId]; ok {
		logrus.Info("in bidirectional remove for client: ", c.Id, "with hub Id: ", hubId)
		c.Hubs[hubId].Unregister <- c
		logrus.Info("after unregister from hub ", hubId)
		delete(c.Hubs, hubId)
		return nil
	} else {
		return fmt.Errorf("hubId not found for delete hub from client hub hubId : ", hubId)
	}
}

//delete hub from the client
func (c *Client) unidirectionalRemoveFromHub(hubId string) error {
	if _, ok := c.Hubs[hubId]; ok {
		delete(c.Hubs, hubId)
		return nil
	} else {
		return fmt.Errorf("hub Id not found for delete hub from client hub hubId : ", hubId)
	}
}
