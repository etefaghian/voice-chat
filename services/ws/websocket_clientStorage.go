package ws

//define a place for storing address of Clients
//if a Client exists in Client storage means that the associated go routing is running
//map from id and address of Client
var clientStorage = make(map[string]*Client)

func addClientToClientStorage(id string, client *Client) {
	clientStorage[id] = client
}
func removeClientFromClientStorage(id string) {
	delete(clientStorage, id)
}
func existsClientInClientStorage(id string) bool {
	temp := clientStorage[id]
	if temp == nil {
		return false
	}
	return true
}

//is not safe if client not exist
func readClientFromClientStorage(id string) *Client {
	return clientStorage[id]
}
