package ws

//for external use

import (
	"fmt"
	"github.com/sirupsen/logrus"
)

//read client from web socket
//does not read from  db
func ReadClientFromWS(clientId string) (*Client, error) {
	//find clientPointer
	//with considering a clientPointer have unique clientId
	if existsClientInClientStorage(clientId) {
		return readClientFromClientStorage(clientId), nil
	}
	return nil, fmt.Errorf("client not found in websocket")
}

//if client already register to hub add hub to it
//else do not do things
func AddAHubToClientsInWs(hubId string, clientsId []string) error {
	for _, client := range clientsId {
		clientPointer, err := ReadClientFromWS(client)
		//if exist in ws
		if clientPointer != nil {
			err = clientPointer.bidirectionalAddToAHub(hubId)
			if err != nil {
				logrus.Error(err)
				return err
			}
		}
	}
	return nil
}

//read client from web socket
//does not read from  db
func ReadHubFromWS(hubId string) (*hub, error) {
	//find hubPointer
	if existsHubInHubStorage(hubId) {
		return readHubFromHubStorage(hubId), nil
	}
	return nil, fmt.Errorf("client not found in websocket")
}

//remove hub from ws
//remove hub from hub storage
func RemoveHubFromWs(hubId string) error {
	hubPointer, err := ReadHubFromWS(hubId)
	if err != nil || hubPointer == nil {
		return fmt.Errorf("hub not found in websocket")
	} else {
		removeHubFromHubStorage(hubId)
		hubPointer.terminateExecution()
		return nil
	}

}
