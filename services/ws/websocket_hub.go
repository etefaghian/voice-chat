package ws

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"voice-chat/dao"
	"voice-chat/shared/models"
)

type hub struct {
	// Registered Clients
	//id and address
	//active in hub
	Clients map[string]*Client
	// Inbound messages from the Clients.
	Broadcast chan models.GeneralMessageModel
	// Register requests from the Clients.
	Register chan *Client
	// Unregister requests from Clients.
	Unregister chan *Client
	//terminateExecution goroutine
	closeChannel chan interface{}
	//identifier for hub
	Id string
	//name for this hub
	Name string
	//operator id
	OperatorId string
	//admin id
	AdminId string
	//list of constraints
	//already init in constructor tight coupling
	constraints map[string]func(hub *hub, model models.GeneralMessageModel) (models.GeneralMessageModel, error)
}

func (h *hub) terminateExecution() {
	h.closeChannel <- 1
}

//add new hub if is not exist else return address of the hub that exists
func serveHub(id string, name string, operatorId string, adminId string, constraintsId []string) *hub {
	if existsHubInHubStorage(id) {
		return readHubFromHubStorage(id)
	} else {
		hub := &hub{
			Clients:      make(map[string]*Client),
			Broadcast:    make(chan models.GeneralMessageModel),
			Register:     make(chan *Client),
			Unregister:   make(chan *Client),
			closeChannel: make(chan interface{}),
			Id:           id,
			Name:         name,
			OperatorId:   operatorId,
			AdminId:      adminId,
			constraints:  make(map[string]func(hub *hub, model models.GeneralMessageModel) (models.GeneralMessageModel, error)),
		}
		addHubToHubStorage(hub.Id, hub) //add to map that store Hubs and id
		for _, constraintId := range constraintsId {
			//read the constraint from constraints storage
			//add it to  the hub if exists
			err := hub.AddConstraint(constraintId)
			if err != nil {
				logrus.Error(err)
			}
		}
		go hub.run() //run hub to handle message and Clients
		return hub   //return address of new hub
	}
}

//read the constraint from constraints storage
//add it to  the hub if exists
func (h *hub) AddConstraint(id string) error {
	if existsConstraint(id) {
		h.constraints[id] = readConstraint(id)
		return nil
	}
	return fmt.Errorf("constraint with id : " + id + "not found")
}

func (h *hub) setOperatorId(id string) {
	h.OperatorId = id
}

func (h *hub) setAdminId(id string) {
	h.AdminId = id
}

//run to handle message or Register members to hub
func (h *hub) run() {
	defer func() {
		logrus.Info("stop hub with id : ", h.Id)
	}()
	for {
		select {
		//add the client to Clients of hub
		case client := <-h.Register:
			h.Clients[client.Id] = client

		//remove client from Clients
		//remove hub from Hubs of client
		case client := <-h.Unregister:
			if _, ok := h.Clients[client.Id]; ok {
				delete(h.Clients, client.Id)
				logrus.Info("unregister for client: ", client.Id)
			}

		//terminateExecution hub and stop associated goroutine
		//first we unregister all client that in Clients map
		//second return means exit from the goroutine
		case <-h.closeChannel:
			for _, client := range h.Clients {
				err := client.unidirectionalRemoveFromHub(h.Id)
				if err != nil {
					logrus.Error(err)
				}
			}

			return
		//handle message
		case message := <-h.Broadcast:
			receiverId := message.ReceiverId // use in section 2 (private message)
			if receiverId == "" {            //means message is  public in this hub
				go h.handlePublicMessage(message)
			} else { //private message
				go h.handlePrivateMessage(message)
			}
		default:
		}
	}
}

//handle public message
//send it to all except sender
func (h *hub) handlePublicMessage(message models.GeneralMessageModel) {
	logrus.Info("public message that it from client: ", message.SenderId)
	for _, client := range h.Clients {
		if message.SenderId == client.Id {
			continue // ignore to send message to sender
		}
		select {
		case client.send <- message:
		//default mean we can not send message
		default:
			client.BidirectionalRemoveFromHub(h.Id)

		}
	}
	go dao.AddPublicMessageToHub(message.HubId, message) //goroutine!!!!!
}

//handle private message
//send it to special client
func (h *hub) handlePrivateMessage(message models.GeneralMessageModel) {
	logrus.Info("private message that it from client: ", message.SenderId)

	//condition 1 violate policy
	//check constraints
	err := h.checkConstraints(message)
	if err != nil {
		logrus.Error(err)
		return
	}

	//condition 2 client not found in online clients
	if _, ok := h.Clients[message.ReceiverId]; !ok { //not found in clients arr maybe client be offline
		if dao.ExistsHubForClient(message.ReceiverId, message.HubId) { //client is offline
			go dao.AddPrivateMessageToHub(message.HubId, message)
			errMessage := generateReceiverFoundButNotConnectedErrorMessage(message)
			select {
			case h.Clients[errMessage.SenderId].send <- errMessage:
			default:
				h.Clients[message.SenderId].BidirectionalRemoveFromHub(h.Id)
			}
		} else { // receiver not found in ws and db
			errMessage := generateReceiverNotFoundErrorMessage(message)
			select {
			case h.Clients[errMessage.SenderId].send <- errMessage:
			default:
				h.Clients[message.SenderId].BidirectionalRemoveFromHub(h.Id)
			}
		}
		return
	}

	//condition 3 all thing is ok
	select {
	case h.Clients[message.ReceiverId].send <- message:
		go dao.AddPrivateMessageToHub(message.HubId, message) //goroutine!!!!! in order to store message
	default: //default mean we can not send message
		close(h.Clients[message.ReceiverId].send)
		h.Unregister <- h.Clients[message.ReceiverId]
	}
}

//check constraint the hub for a message
func (h *hub) checkConstraints(m models.GeneralMessageModel) error {
	for _, constraint := range h.constraints {
		if errMessage, err := constraint(h, m); err != nil { //violate policy ignore message and send an error to sender
			select {
			case h.Clients[m.SenderId].send <- errMessage: //send error message to sender
			default:
				h.Clients[m.SenderId].BidirectionalRemoveFromHub(h.Id)
			}
			return fmt.Errorf("constraint was violated")
		}
	}
	return nil
}
