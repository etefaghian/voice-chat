package ws

//define a place for storing address of Hubs
//if a hub exists in hub storage means that the associated go routing is running
//map from id and address of hub
var hubStorage = make(map[string]*hub)

func addHubToHubStorage(id string, hub *hub) {
	hubStorage[id] = hub
}
func removeHubFromHubStorage(id string) {
	delete(hubStorage, id)
}
func existsHubInHubStorage(id string) bool {
	temp := hubStorage[id]
	if temp == nil {
		return false
	}
	return true
}

func readHubFromHubStorage(id string) *hub {
	return hubStorage[id]
}
