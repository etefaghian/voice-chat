package ws

//

import "voice-chat/shared/models"

//read from ws
func StatusOfHubsInWS() ([]models.HubModel, error) {
	hubs := make([]models.HubModel, 0)
	for _, hp := range hubStorage {
		//get clients id
		clientsId := make([]string, 0)
		for id, _ := range hp.Clients {
			clientsId = append(clientsId, id)
		}
		//get constraints id
		constraintsId := make([]string, 0)
		for id, _ := range hp.constraints {
			constraintsId = append(constraintsId, id)
		}
		hub := models.HubModel{
			Id:            hp.Id,
			Name:          hp.Name,
			ClientsId:     clientsId,
			OperatorId:    hp.OperatorId,
			AdminId:       hp.AdminId,
			ConstraintsId: constraintsId,
		}
		hubs = append(hubs, hub)
	}

	return hubs, nil
}

func StatusOfClientsInWS() ([]models.ClientModel, error) {
	clients := make([]models.ClientModel, 0)
	for _, cp := range clientStorage {
		//make hubs id
		hubsId := make([]string, 0)
		for id, _ := range cp.Hubs {
			hubsId = append(hubsId, id)
		}
		client := models.ClientModel{
			Id:     cp.Id,
			Name:   cp.Name,
			HubsId: hubsId,
		}
		clients = append(clients, client)
	}

	return clients, nil

}
