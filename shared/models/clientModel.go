package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ClientModel struct {
	Id        string    `json:"id" bson:"_id"`
	Name      string    `json:"name" bson:"name"`
	HubsId    []string  `json:"hubs_Id" bson:"hubs_id"`
	Timestamp time.Time `json:"timestamp" bson:"timestamp"`
	Username  string    `json:"username" bson:"username"`
	Password  string    `json:"password" bson:"password"`
	Role      string    `json:"role" bson:"role"`
}

//fill client id when we add client to hub
func NewClientModel(name, username, password string, role string) ClientModel {
	return ClientModel{
		Id:        primitive.NewObjectID().Hex(),
		Name:      name,
		HubsId:    make([]string, 0),
		Timestamp: time.Now(),
		Username:  username,
		Password:  password,
		Role:      role,
	}
}
