package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

const (
	Image = "img"
)

type FileModel struct {
	Id        string    `json:"id" bson:"_id"`
	Name      string    `json:"name" bson:"name"`
	ClientId  string    `json:"client_id" bson:"client_id"`
	Type      string    `json:"type" bson:"type"`
	Timestamp time.Time `json:"timestamp" bson:"timestamp"`
}

func NewImageModel(name string, clientId string) FileModel {
	return FileModel{
		Id:        primitive.NewObjectID().Hex(),
		Name:      name,
		Type:      Image,
		Timestamp: time.Now(),
		ClientId:  clientId,
	}
}
