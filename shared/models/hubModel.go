package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type HubModel struct {
	Id              string                `bson:"_id" json:"id"`
	Name            string                `bson:"name" json:"name"`
	Timestamp       time.Time             `json:"timestamp" bson:"timestamp"`
	PublicMessages  []GeneralMessageModel `json:"public_messages" bson:"public_messages"`   //public in channel
	PrivateMessages []GeneralMessageModel `json:"private_messages" bson:"private_messages"` //peer  to peer
	ClientsId       []string              `json:"clients_id" bson:"clients_id"`
	OperatorId      string                `json:"operator_id" bson:"operator_id"`
	AdminId         string                `json:"admin_id" bson:"admin_id"`
	ConstraintsId   []string              `json:"constraints_id" bson:"constraints_id"`
}

func NewHubModel(name string, clientsId []string, operatorId string, adminId string, constraintsId []string) HubModel {
	return HubModel{
		Id:              primitive.NewObjectID().Hex(),
		Name:            name,
		Timestamp:       time.Now(),
		PublicMessages:  make([]GeneralMessageModel, 0),
		PrivateMessages: make([]GeneralMessageModel, 0),
		ClientsId:       clientsId,
		OperatorId:      operatorId,
		AdminId:         adminId,
		ConstraintsId:   constraintsId,
	}
}
