package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type GeneralMessageModel struct {
	Id         string      `json:"id" bson:"_id"`
	SenderId   string      `json:"sender_id" bson:"sender_id"`
	ReceiverId string      `json:"receiver_id" bson:"receiver_id"`
	HubId      string      `json:"hub_id" bson:"hub_id"`
	Code       int         `json:"code" bson:"code"`
	Body       interface{} `json:"body" bson:"body"`
	Timestamp  time.Time   `bson:"timestamp" json:"timestamp"`
}

func NewGeneralMessageModel(senderId string, receiverId string, hubId string, code int, body interface{}) GeneralMessageModel {
	return GeneralMessageModel{SenderId: senderId,
		ReceiverId: receiverId,
		HubId:      hubId,
		Code:       code,
		Body:       body,
		Timestamp:  time.Now(),
		Id:         primitive.NewObjectID().Hex()}
}

type StreamMessage []byte
